#!/usr/bin/env python3

##########################################
## IMPORTS                              ##
##########################################

import os
import time
import glob
import json
import requests
import bs4
from displayer import html_specific, html_index
from tools import print_log_line
from error import error

##########################################
## FUNCTIONS                            ##
##########################################

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return (func)
    return (decorate)

def trim(s):
    s = s.strip()
    s = s.replace(' ', '')
    s = s.replace('\n', '')
    s = s.replace('\t', '')
    return (s)

def bourso_prefix(code):
    if not code:
        return (None)
    url = 'https://www.boursorama.com/bourse/action/graph/ws/UpdateCharts'
    for prefix in ['1rP', '1rA', '1rT', '1rEP', '1g', '1z', '6s', '6p', '2a']:
        tmp = requests.get(url + '?symbol=' + prefix + code)
        if tmp.status_code == 200 and tmp.json():
            return (prefix)
    error("No prefix found for value '%s'. Please specify one." % code)
    return (None)

def bourso_name(prefix, code):
    if not prefix or not code:
        return (None)
    url = 'https://www.boursorama.com/cours/' + str(prefix) + str(code) + '/'
    res = requests.get(url)
    if res.status_code == 200:
        html = bs4.BeautifulSoup(res.text, features="html.parser")
        name = html.find('a', {'class':'c-faceplate__company-link'}).text
        name = name.strip()
    return (name)

def bourso_isin(prefix, code):
    if not prefix or not code:
        return (None)
    url = 'https://www.boursorama.com/cours/' + str(prefix) + str(code) + '/'
    res = requests.get(url)
    if res.status_code == 200:
        html = bs4.BeautifulSoup(res.text, features="html.parser")
        isin = html.find('h2', {'class':'c-faceplate__isin'})
        if isin is None:
            return None
        isin = isin.text.split(' ')[0]
    return (isin)

def bourso_currency(prefix, code):
    if not prefix or not code:
        return (None)
    url = 'https://www.boursorama.com/cours/' + str(prefix) + str(code) + '/'
    res = requests.get(url)
    if res.status_code == 200:
        html = bs4.BeautifulSoup(res.text, features="html.parser")
        curr = html.find('span', {'class':'c-faceplate__price-currency'})
        if curr is None:
            return None
        curr = curr.text.strip()
    return (curr)

@static_vars(cache={})
def bourso_price(prefix, code):
    if not prefix or not code:
        return (None)
    url = 'https://www.boursorama.com/bourse/action/graph/ws/UpdateCharts'
    if code not in bourso_price.cache:
        try:
            tmp = requests.get(url + '?symbol=' + prefix + code)
            if tmp.status_code == 200:
                infos = tmp.json()
                if infos != []: # empty result
                    bourso_price.cache[code] = infos['c']
        except requests.exceptions.ConnectionError:
            error("Connection failed, cannot get price of '%s'." % code, quit=0)
            time.sleep(1)
            bourso_price.cache[code] = bourso_price(prefix, code)
    return (bourso_price.cache[code])

def bourso_year_block(html, block):
    top, bot = (None, None)
    span = block.find('span', {'class':'c-heading__text'})
    if span and 'Données historiques' in span.text:
        rows = html.findAll('tr', {'class':'c-table__row'})
        for r in rows:
            if 'Janvier' in r.text:
                # shit for 1er janvier
                cells = r.findAll('td', {'class':'c-table__cell'})
                try:
                    top = float(trim(cells[2].text))
                    bot = float(trim(cells[3].text))
                    print_log_line("  Year top is %.02f / bot is %.02f" % (top, bot))
                except:
                    print_log_line("  Year top/bot bot found")
                    pass
    return (top, bot)

@static_vars(cache={})
def bourso_year(prefix, code):
    if code not in bourso_year.cache:
        bourso_year.cache[code] = (None, None)
        url = 'https://www.boursorama.com/cours/' + prefix + code + '/'
        res = requests.get(url)
        if res.status_code == 200:
            html = bs4.BeautifulSoup(res.text, features="html.parser")
            blocks = html.findAll('div', {'class':'c-block'})
            for block in blocks:
                top, bot = bourso_year_block(html, block)
                if top and bot:
                    bourso_year.cache[code] = (top, bot)
    return (bourso_year.cache[code])

def bourso_write_prices():
    print_log_line("Sauvegarde des prix :")
    folders = ['.', 'data', 'valeurs']
    path = ""
    for f in folders:
        path += (f + '/')
        if not os.path.isdir(path):
            os.mkdir(path)
            print_log_line('MKDIR ' + str(path))
    for code in bourso_price.cache:
        with open('./data/valeurs/' + code + '.csv', 'a') as fd:
            ts = str(time.time()).split('.')[0]
            fd.write(ts + ',' + str(bourso_price.cache[code]) + '\n')
            print_log_line(f" -> {code} {bourso_price.cache[code]}")
            fd.close()
    print_log_line("%i prix sauvegardé(s)." % len(bourso_price.cache))
