#!/usr/bin/env python3

##########################################
## IMPORTS                              ##
##########################################

import enum
import os
import datetime
import random

##########################################
## CLASSES                              ##
##########################################

class Secteur(enum.Enum):
    TECHNOLOGIES = 1
    TELECOM = 2
    AUTOMOBILE = 3
    ENERGIE = 4
    CONSOMMATION = 5
    BANCAIRE = 6
    FINANCE = 7
    AEROSPATIAL = 7
    SANTE = 9
    TOURISME = 1
    MATERIAUX = 11
    IMMOBILIER = 12
    SECURITE = 13
    ALIMENTAIRE = 14
    ENVIRONEMENT = 15
    CONSTRUCTION = 16
    JEUXVIDEOS = 17
    LUXE = 18
    DATA = 19
    AGROALIMENTAIRE = 20

##########################################
## VARIABLES                            ##
##########################################

VALEURS = {
    'ACA': [Secteur.BANCAIRE],
    "ABCA": [Secteur.FINANCE],
    "ABIO": [Secteur.ENERGIE],
    "AF": [Secteur.AEROSPATIAL, Secteur.TOURISME],
    "AI": [Secteur.ENERGIE],
    "AIR": [Secteur.AEROSPATIAL],
    "ALAGP": [Secteur.ENERGIE],
    "ALAQU": [Secteur.SECURITE],
    "ALDRV": [Secteur.AEROSPATIAL],
    "ALFOC": [],
    "ALHYG": [],
    "ALO": [Secteur.CONSTRUCTION],
    "ALUMS": [Secteur.TECHNOLOGIES],
    "AM": [Secteur.AEROSPATIAL],
    "AMS": [Secteur.TECHNOLOGIES],
    "BN": [Secteur.CONSOMMATION],
    "BON": [Secteur.CONSOMMATION],
    "CDA": [Secteur.TOURISME],
    "CRBP2": [Secteur.BANCAIRE],
    "CRI": [Secteur.SANTE],
    "CS": [Secteur.FINANCE],
    "DBG": [Secteur.ENVIRONEMENT],
    "DIM": [],
    "DSY": [Secteur.TECHNOLOGIES],
    "EDF": [Secteur.ENERGIE],
    "EIFF": [Secteur.TOURISME],
    "ELIOR": [],
    "EN": [Secteur.CONSTRUCTION],
    "ENGI": [Secteur.ENERGIE],
    "EWLD": [],
    "FGA": [Secteur.AEROSPATIAL],
    "FGR": [],
    "FP": [Secteur.ENERGIE],
    "GUI": [Secteur.JEUXVIDEOS, Secteur.TECHNOLOGIES],
    "HO": [Secteur.TECHNOLOGIES],
    "ILD": [Secteur.TELECOM],
    "LAT": [Secteur.AEROSPATIAL],
    "LFDE": [Secteur.ENERGIE],
    "MAU": [Secteur.ENERGIE],
    "MC": [Secteur.LUXE],
    "MCPHY": [Secteur.ENERGIE, Secteur.ENVIRONEMENT],
    "MERY": [Secteur.IMMOBILIER],
    "OR": [Secteur.LUXE],
    "ORA": [Secteur.TELECOM],
    "ORAP": [Secteur.SANTE],
    "PHAG": [],
    "PHAU": [],
    "PRX": [Secteur.DATA, Secteur.TECHNOLOGIES],
    "PUB": [],
    "PUST": [],
    "RIN": [Secteur.AGROALIMENTAIRE],
    "RNO": [Secteur.AUTOMOBILE],
    "RXL": [],
    "SEV": [Secteur.ENERGIE],
    "SMTPC": [Secteur.AUTOMOBILE],
    "UBI": [Secteur.TECHNOLOGIES],
    "UG": [Secteur.AUTOMOBILE],
    "URW": [Secteur.IMMOBILIER],
    "VIE": [Secteur.ENERGIE],
    "VOW": [Secteur.AUTOMOBILE],
}

COLORS = ['red','blue','green','orange','yellow','purple','black','grey']
COLORS_UNUSED = []

##########################################
## FUNCTIONS                            ##
##########################################

def get_template(filename='./charts/templates/secteur.html'):
    template = ""
    with open(filename, 'r') as fd:
        template = fd.read()
    return (template)

def get_csv(path='./data/valeurs/'):
    csv = []
    for root, dirs, files in os.walk("./data/valeurs/"):
        for filename in files:
            if filename[-4::] == '.csv':
                csv.append(filename)
    return (csv)

def secteurs():
    classement = {}
    values = get_csv()
    for v in values:
        elem = v[:-4:]
        if elem in VALEURS.keys():
            for sect in VALEURS[elem]:
                if sect not in classement.keys():
                    classement[sect] = []
                classement[sect].append(elem)
    template = get_template()
    for k, v in classement.items(): # each sector
        COLORS_UNUSED = COLORS
        name = str(k).split('.')[1]
        print("%03d" % len(v), 'element(s) dans', name)
        datasets = ''
        labels = []
        _tmp = []
        for _value in v: # each value
            _d, _t = set_dataset(_value)
            _tmp.append(_d)
            labels += _t
        _secteur = '[' + ','.join(_tmp) + ']'
        datasets = _secteur
        html = template % {
            'NAME': name,
            'DATASETS': datasets,
            'LABELS': sorted(set(labels)),
        }
        with open('./charts/secteurs/' + name + '.html', 'w') as fd:
            fd.write(html)

def timestamp_to_date_format(ts):
    date = datetime.datetime.fromtimestamp(int(ts))
    return (date.strftime("%Y-%m-%d"))

def pick_random_color():
    random_number = random. randint(0,16777215)
    hex_number = str(hex(random_number))
    return ('#' + hex_number[2:])

def set_dataset(name):
    _buf = ""
    prix = []
    timeline = []
    with open('./data/valeurs/' + name + '.csv') as fd:
        _buf = fd.read()
    for line in _buf.split('\n'):
        _sp = line.split(',')
        if len(_sp) == 2:
            _time = timestamp_to_date_format(_sp[0])
            _prix = float(_sp[1])
            if _time not in timeline:
                prix.append(_prix)
                timeline.append(_time)
    color = pick_random_color()
    data = """{
        label: '%(NAME)s',
        fill: false,
        //backgroundColor: window.chartColors.%(COLOR)s,
        //borderColor: window.chartColors.%(COLOR)s,
        backgroundColor: '%(COLOR)s',
        borderColor: '%(COLOR)s',
        data: %(PRIX)s,
    }
    """ % {
        'NAME': name,
        'PRIX': prix,
        'COLOR': color,
    }
    return (data, timeline)


##########################################
## ENTRYPOINT                           ##
##########################################

if __name__ == '__main__':
    secteurs()

