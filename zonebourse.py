#!/usr/bin/env python3

##########################################
## IMPORTS                              ##
##########################################

import requests
import sys
import bs4

##########################################
## VARIABLES                            ##
##########################################

ZB_ROOT = 'https://www.zonebourse.com'

##########################################
## FUNCTIONS                            ##
##########################################

def zb_infos(elem):
    res = requests.get(elem['page'])
    html = bs4.BeautifulSoup(res.text, features="html.parser")
    elem['price'] = html.find('td', {'class':'fvPrice'}).text
    elem['price'] = float(elem['price'])
    elem['curr'] = html.find('td', {'class':'fvCur'}).text
    elem['daily'] = html.find('td', {'class':'fvPerf'}).text
    return (elem)

def zb_results(raw_html):
    results = []
    html = bs4.BeautifulSoup(raw_html, features="html.parser")
    table = html.find('table', {'id':'ALNI0'})
    elems = table.findAll('tr')
    for el in elems:
        tds = el.findAll('td')
        link = el.find('a')
        code = tds[0].text
        if code != 'Code' and link != None:
            name = tds[2].text.split('\xa0')[0]
            curr = tds[2].text.split('\xa0')[1]
            tmp = {
                'code': code,
                'name': name,
                'page': ZB_ROOT + link.attrs['href'],
                'curr': curr
            }
            results.append(tmp)
    return (results)

def zb_query(isin):
    url_query = ZB_ROOT + '/recherche/instruments/?aComposeInputSearch=s_%s'
    url = url_query % isin
    res = requests.get(url)
    results = zb_results(res.text)
    for elem in results:
        infos = zb_infos(elem)
        if 'price' in infos:
            infos['isin'] = isin
            infos['prefix'] = 'ZB'
            return(infos)
    return (None)


##########################################
## ENTRYPOINT                           ##
##########################################

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()
    else:
        for isin in sys.argv[1:]:
            value = zb_query(isin)
            print(value)
