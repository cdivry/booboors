#!/usr/bin/env python3

##########################################
## IMPORTS                              ##
##########################################

import os
import time
import glob
import json
import requests
import bs4
from displayer import html_specific, html_index
from error import error
from tools import print_log_line

from bourso import bourso_prefix
from bourso import bourso_price
from bourso import bourso_name
from bourso import bourso_isin
from bourso import bourso_currency
from bourso import bourso_year
from bourso import bourso_write_prices
from zonebourse import zb_query

##########################################
## FUNCTIONS                            ##
##########################################

def value_stats(value):
    # ZB / Bourso
    if value['prefix'] == 'ZB':
        zb = zb_query(value['isin'])
        value['price'] = zb['price']
    else:
        value['price'] = bourso_price(value['prefix'], value['code'])

    # top / bottom
    if ('top' not in value or 'bot' not in value) and value['prefix'] != 'ZB':
        value['top'], value['bot'] = bourso_year(value['prefix'], value['code'])
        print_log_line("  Value top/bottom for current year '%s' updated." % value['name'])

    if 'pru' not in value or not value['pru']:
        value['pru'] = value['price']

    value['valo'] = value['price'] * float(value['amount'])
    value['pv'] = value['valo'] - (value['pru'] * float(value['amount']))
    value['perf'] = ((value['price'] * 100.0) / value['pru']) - 100.0
    #print_log_line(json.dumps(value, sort_keys=True, indent=4))

    print_log_line("  Value stats (price, valorisation, pv, performance) updated.")
    return (value)

def value_name(value):
    if 'name' not in value or not value['name']:
        if value['prefix'] == 'ZB':
            zb = zb_query(value['isin'])
            if zb and 'name' in zb: # correct feed
                value['name'] = zb['name']
                print_log_line("  Value name '%s' updated." % value['name'])
        else:
            value['name'] = bourso_name(value['prefix'], value['code'])
            if value['name']:
                print_log_line("  Value name '%s' updated." % value['name'])
    return (value)

def value_isin(value):
    if 'isin' not in value or not value['isin']:
        value['isin'] = bourso_isin(value['prefix'], value['code'])
        print_log_line("  Value ISIN '%s' updated." % value['isin'])
    return (value)

def value_currency(value):
    if 'currency' not in value or not value['currency']:
        if value['prefix'] == 'ZB':
            zb = zb_query(value['isin'])
            if zb and 'curr' in zb:
                value['currency'] = zb['curr']
                print_log_line("  Value currency '%s' updated." % value['currency'])
        else:
            value['currency'] = bourso_currency(value['prefix'], value['code'])
            if value['currency']:
                print_log_line("  Value currency '%s' updated." % value['currency'])
    return (value)

def value_amount(value):
    if 'amount' not in value or not value['amount']:
        value['amount'] = 1
        print_log_line("  Value amount '%d' updated." % value['amount'])
    return (value)

def value_prefix(value):
    if 'prefix' not in value or not value['prefix']:
        value['prefix'] = bourso_prefix(value['code'])
        if value['prefix']:
            print_log_line("  Value prefix '%s' updated." % value['prefix'])
    if not value['prefix']:
        zb = zb_query(value['isin'])
        if zb and 'price' in zb: # correct feed
            value['prefix'] = 'ZB'
            value['code'] = zb['code']
            print_log_line("  Value prefix replaced to 'ZB'.")
    return (value)

def value_skipping(value):
    print_log_line("Skipping value '%s' ..." % value['code'])
    value['price'] = value['pru']
    value['perf'] = 0.00
    value['pv'] = 0.00
    value['valo'] = value['price'] * value['amount']
    if 'name' not in value or not value['name']:
        value['name'] = "???"
    value['prefix'] = None
    return (value)

def value_update(value):
    try:
        if 'code' not in value or not value['code']:
            return (value_skipping(value))

        print_log_line("Updating value '%s' ..." % value['code'])
        value = value_prefix(value)
        value = value_name(value)
        value = value_isin(value)
        value = value_amount(value)
        value = value_currency(value)

        value = value_stats(value)
        print_log_line("Value '%s' updated." % value['code'])
    except Exception as e:
        print_log_line("value_update ERROR:", e)
        print_log_line("value_update ERROR: retrying")
        value = value_update(value)
    return (value)

def wallet_files(folder):
    if folder[-1] != '/':
        folder += '/'
    if not os.path.isdir(folder):
        print_log_line("Erreur lors du parcours du dossier '%s'" % folder)
        return ([])
    res = glob.glob(folder + '*.json')
    print_log_line("Portefeuille(s): %d" % len(res))
    for w in res:
        print_log_line(w)
    return (res)

def wallet_stats(wallet):
    return (wallet)

def wallet_update(filename):
    with open(filename, 'r') as fd:
        try:
            raw_data = fd.read()
            # remove randomly added character, causing issues
            raw_data = raw_data[:-1] if raw_data[-2:] == "}}" else raw_data
            wallet = json.loads(raw_data)
        except json.decoder.JSONDecodeError as err:
            error(filename + ": " + str(err))
        wallet['filename'] = filename.split('/')[-1]
        if 'versements' not in wallet:
            wallet['versements'] = 0.0
        if 'especes' not in wallet:
            wallet['especes'] = 0.0
        values = wallet['values'] if 'values' in wallet else []
        updated_values = []
        for v in values:
            v = value_update(v)
            updated_values.append(v)
        updated_wallet = {
            "name": wallet['name'],
            "filename": filename.split('/')[-1],
            "updated": time.time(),
            "values": sorted(updated_values, key=lambda k: k['perf'], reverse=True),
            "titres": sum([v['price'] * v['amount'] for v in updated_values]),
            "especes": wallet['especes'],
            "versements": wallet['versements']
        }
        updated_wallet['total'] = updated_wallet['especes'] + updated_wallet['titres']
        if updated_wallet['versements']: # avoid division by zero
            updated_wallet['perf'] = ((updated_wallet['total'] * 100.0) / updated_wallet['versements']) - 100.0
        else:
            updated_wallet['perf'] = 0.0
        with open(filename, 'w') as fd:
            #data = json.dumps(updated_wallet, sort_keys=True, indent=4)
            data = json.dumps(updated_wallet, indent=4)
            fd.write(data) # this randomly add another } at EOF
            print_log_line("Wallet '%s' updated." % filename)
        #print_log_line(json.dumps(updated_wallet, sort_keys=True, indent=4))
        html_file = filename.replace('.json', '.html')
        html_file = html_file.replace('./bank/', './www/')
        html = html_specific(updated_wallet)
        with open(html_file, 'w') as fd:
            fd.write(html)
            print_log_line("HTML page '%s' updated." % html_file)
        # save wallet time/values
        if not os.path.exists('./data/'):
            os.mkdir('./data/')
        if not os.path.exists('./data/portefeuilles/'):
            os.mkdir('./data/portefeuilles/')
        stats_out = './data/portefeuilles/' + updated_wallet['filename'].replace('.json', '.csv')
        if not os.path.isfile(stats_out):
            with open(stats_out, 'a') as fd:
                fd.write("date,total,especes,titres,versements" + '\n')
        with open(stats_out, 'a') as fd:
            fd.write("%s,%s,%s,%s,%s" % (
                "%d" % int(updated_wallet['updated']),
                "%.2f" % updated_wallet['total'],
                "%.2f" % updated_wallet['especes'],
                "%.2f" % updated_wallet['titres'],
                "%.2f" % updated_wallet['versements']
            ) + '\n')
        return (updated_wallet)

##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':
    wallets = []
    filenames = wallet_files('./bank')
    for filename in filenames:
        wallets.append(wallet_update(filename))
    html = html_index(wallets)
    index = './www/index.html'
    with open(index, 'w') as fd:
        fd.write(html)
        print_log_line("HTML index '%s' updated." % index)
    bourso_write_prices()
