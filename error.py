
def error(msg, quit=1):
    print('\033[31m', end='')
    print(msg, end='')
    print('\033[0m')
    if quit:
        exit(-1)
