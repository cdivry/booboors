#!/usr/bin/env python3

##########################################
## IMPORTS                              ##
##########################################

import json
import os
import time
import datetime
import glob
from html import escape as html_escape
import sys

##########################################
## FUNCTIONS                            ##
##########################################

def list_wallet_files(folder):
    if folder[-1] != '/':
        folder += '/'
    if not os.path.isdir(folder):
        print("Erreur lors du parcours du dossier '%s'" % folder)
        return ([])
    res = glob.glob(folder + '*.json')
    return (res)

def list_values_files(folder):
    if folder[-1] != '/':
        folder += '/'
    if not os.path.isdir(folder):
        print("Erreur lors du parcours du dossier '%s'" % folder)
        return ([])
    res = glob.glob(folder + '*.csv')
    return (res)

def template_get(filename):
    with open(filename, 'r') as fd:
        template = fd.read()
    return (template)

def template_wallet(filename = './charts/templates/wallet.html'):
    return (template_get(filename))

def template_value(filename = './charts/templates/value.html'):
    return (template_get(filename))

def get_value(filename):
    with open(filename, 'r') as fd:
        lines = fd.read().split('\n')
    for remove in ['date,prix', '']:
        if remove in lines:
            lines.remove(remove)
    stamps = []
    for line in lines:
        csv_line = line.split(',')
        #print(csv_line)
        stamps.append({
            'date': int(csv_line[0]),
            'prix': float(csv_line[1]),
        })
    return (stamps)

def get_wallet(filename):
    with open(filename, 'r') as fd:
        wallet = json.load(fd)
    return (wallet)

def get_wallet_stamps(filename):
    print("Reading wallet: %s" % filename)
    try:
        with open(filename, 'r') as fd:
            lines = fd.read().split('\n')
    except:
        print("Error reading filename: maybe chmod, or run ./crawler.py first.")
        sys.exit(0)
    for remove in ['date,total,especes,titres,versements', '']:
        if remove in lines:
            lines.remove(remove)
    stamps = []
    for line in lines:
        csv_line = line.split(',')
        #print(csv_line)
        stamps.append({
            'date': int(csv_line[0]),
            'total': float(csv_line[1]),
            'especes': float(csv_line[2]),
            'titres': float(csv_line[3]),
            'versements': float(csv_line[4]),
        })
    return (stamps)

def write_html(filename, data):
    # check if mkdir needed
    folders = filename.split('/')[:-1:]
    #print(folders)
    path = ""
    for f in folders:
        path += (f + '/')
        if not os.path.isdir(path):
            os.mkdir(path)
            print('MKDIR', path)
    # write output
    with open(filename, 'w') as fd:
        fd.write(data)
    print('chart', filename, 'written.')

def stamp_to_date(stamp):
    day = datetime.datetime.fromtimestamp(stamp)
    date = day.strftime("%Y-%m-%d")
    return (date)

def date_to_stamp(date):
    stamp = datetime.datetime.timestamp(date)
    return (stamp)

def date_in_year(ts):
    now = time.time()
    year = datetime.datetime.now().year
    january_1st = datetime.datetime.timestamp(
        datetime.datetime(year, 1, 1, 0, 0)
    )
    if (ts >= january_1st and ts <= now):
        return (True)
    return (False)

def get_stamps_ytd(stamps):
    stamps = sorted(stamps, key=lambda k: k['date'], reverse=False)
    res = {}
    year = datetime.datetime.now().year
    now = datetime.datetime.now()
    for s in stamps:
        day = datetime.datetime.fromtimestamp(s['date'])
        key = day.strftime("%Y-%m-%d")
        value = s
        if date_in_year(s['date']):
            res[key] = value
    return (res)

def draw_charts_wallet(filename):
    wallet = get_wallet(filename)
    template = template_wallet()
    stamps = get_wallet_stamps('./data/portefeuilles/' + wallet['filename'].replace('.json', '.csv'))
    ytd = get_stamps_ytd(stamps)
    html_template = template % {
        'NAME': html_escape(wallet['name']),
        "TOTAL": str([e['total'] for e in stamps if date_in_year(e['date'])]),
        "ESPECES": str([e['especes'] for e in stamps if date_in_year(e['date'])]),
        "TITRES": str([e['titres'] for e in stamps if date_in_year(e['date'])]),
        "VERSEMENTS": str([e['versements'] for e in stamps if date_in_year(e['date'])]),
        "LABELS": str(list(ytd.keys())),
    }
    html_out = './charts/portefeuilles/' + wallet['filename'].replace('.json', '.html')
    write_html(html_out, html_template)
    return (html_template)

def draw_charts_value(filename):
    name = filename.split('/')[::-1][0].split('.csv')[0]
    stamps = get_value(filename)
    template = template_value()
    ytd = get_stamps_ytd(stamps)
    prix = str([e['prix'] for e in stamps if date_in_year(e['date'])])
    labs = str([stamp_to_date(e['date']) for e in stamps if date_in_year(e['date'])])
    html_template = template % {
        "NAME": name,
        "PRIX": prix,
        "LABELS": labs
    }
    html_out = './charts/valeurs/' + name + '.html'
    write_html(html_out, html_template)
    return (name, prix, labs)

def draw_charts_values():
    values = list_values_files('./data/valeurs/')
    canvas = ""
    script = ""
    onloads = ""
    for v in values:
        name, prix, labs = draw_charts_value(v)
        canvas += "<canvas id='canvas_%(NAME)s'></canvas>" % {'NAME': name}
        scpt_tmpl = template_get('./charts/templates/value_script.html')
        script += scpt_tmpl % {
            "NAME": name,
            "PRIX": prix,
            "LABELS": labs,
        }
        onloads += """
            var ctx_%(NAME)s = document.getElementById('canvas_%(NAME)s');
            window.myLine = new Chart(ctx_%(NAME)s, config_%(NAME)s)
	""" % {'NAME': name}
    html_template = template_get('./charts/templates/values.html') % {
        'CANVAS': canvas,
        'SCRIPT': script,
        'ONLOAD': onloads,
    }
    write_html('./charts/valeurs/index.html', html_template)
    return (html_template)

def draw_charts():
    wallets = list_wallet_files('./bank/')
    html_template = ""
    for w in wallets:
        html_template += draw_charts_wallet(w)
    draw_charts_values()
    write_html('./charts/portefeuilles/index.html', html_template)
    #draw_charts_wallet('./bank/pouet.json')
    #draw_charts_value('./data/valeurs/ENGI.csv')


##########################################
## ENTRYPOINT                           ##
##########################################

if __name__ == '__main__':
    draw_charts()
