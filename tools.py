
import datetime

def get_log_line(message : str):
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return f"[{now}] {message}"

def print_log_line(message : str):
    print(get_log_line(message))
