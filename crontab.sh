#!/bin/bash

# crontab -e
# to specify refresh delay :#
# here is each 5mn, monday to friday, from 9h to 17h59 :
# */5 8-18 * * 1-5 ./crontab.sh >> crontab.log 2>&1

APP_PATH=$(echo $0 | sed -e 's/crontab\.sh//g')

pushd $APP_PATH
. env/bin/activate
./crawler.py
popd
