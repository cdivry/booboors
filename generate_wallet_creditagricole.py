#!/usr/bin/env python3

##########################################
##  INCLUDES                            ##
##########################################

import sys
import time
import datetime
import json

##########################################
##  FUNCTIONS                           ##
##########################################

def ca_fix(value):
    if value['isin'] == "QS0008263806":
        value['isin'] = "FI0009900104"
    return (value)

def usage():
    msg = """
    Usage:
        %s <file1.csv> <file2.csv> <...>"

    Génération des fichiers :
        Depuis l'Investore, allez sur [Portefeuille], puis [Historique].
        Sélectionnez une période pertinente, puis cliquez sur [Exporter].
        Choisissez le format [CSV].
    """ % sys.argv[0]
    print(msg)

def date_str2ts(str_date):
    tmp = datetime.datetime.strptime(str_date, "%d/%m/%Y")
    ts = time.mktime(tmp.timetuple())
    return (ts)

def date_ts2str(ts):
    date = datetime.datetime.fromtimestamp(ts)
    date_str = date.strftime("%d/%m/%Y")
    return (date_str)

def str2float(str_float):
    res = str_float.replace(',', '.')
    res = res.replace(' ', '')
    return (float(res))

def get_wallet_date(lines):
    updated = lines[1].split(' ')[2].split(';')[0]
    ts = date_str2ts(updated)
    return (ts)

def get_line_operation(op):
    if op in ["VERSEMENT SUR COMPTE DEDIE", "VERSEMENT SUR COMPTE PEA"]:
        op = "VERSEMENT"
    elif op in ["RETRAIT DU COMPTE DEDIE", "RETRAIT DU COMPTE PEA"]:
        op = "RETRAIT"
    elif op in ["TAXE TRANSACT FINANC", "TAXE TRANSACTIONS FINANCIERES"]:
        op = "FRAIS"
    elif op in ["ECH AUTO A ROMPU", "ECH AUTO A. ROMPU", "ECH. AUTO A ROMPU"]:
        op = "DIVIDENDE"
    elif op in ["PAIEMENT DE COUPON", "RACHAT HORS BOURSE", "RACHAT H.BOURSE", "REMBOURSEMENT PLAF PEA T.PERCU"]:
        op = "DIVIDENDE"
    elif op in ["ACHAT COMPTANT", "ACH COMPTANT", "ACH ETRANGER", "ACHAT ETRANGER", "SOUSCRIPTION"]:
        op = "ACHAT"
    elif op in ["VENTE COMPTANT", "VTE COMPTANT", "VENTE ETRANGER", "VTE ETRANGER"]:
        op = "VENTE"
    elif op in ["BLOC OST EN COURS", "DETACH. DROITS", "SOUSCRIPTION", "RBT DE CAPITAL/DISTRIB +VALUES"]:
        #shit
        pass
    else:
        print("UNKNOWN TYPE: '%s'" % op)
        exit(0)
    return (op)

def get_wallet_name(lines):
    no_compte = lines[2].split(' ')[3].split(';')[0]
    du = lines[3].split(' ')[1].replace('/', '')
    au = lines[3].split(' ')[3].split(';')[0].replace('/', '')
    name = 'compte_' + no_compte + '_du_' + du + '_au_' + au + '.csv'
    return (name)

def get_wallet_histo(lines):
    histo = []
    orders = lines[5:]
    orders.reverse()
    for line in orders:
        print(line)
        line = line.split(';')
        if len(line) == 12:
            elem = {
                'date': date_str2ts(line[0]),
                'operation': get_line_operation(line[1]),
                'type': line[2],
                'quantite': int(str2float(line[3])),
                'code': line[4],
                'isin': line[5],
                'label': line[6],
                'montant': str2float(line[8]),
                'frais_ttc': line[9],
                'devise': line[10],
                'date_operation': line[11],
            }
            histo.append(elem)
    return (histo)

def wallet_histo_line_dividende(wallet, shares, h):
    wallet['especes'] += h['montant']
    return (wallet, shares)

def wallet_histo_line_frais(wallet, shares, h):
    wallet['especes'] += h['montant']
    return (wallet, shares)

def wallet_histo_line_versement(wallet, shares, h):
    wallet['versements'] += h['montant']
    wallet['especes'] += h['montant']
    return (wallet, shares)

def wallet_histo_line_retrait(wallet, shares, h):
    wallet['versements'] += h['montant']
    wallet['especes'] += h['montant']
    return (wallet, shares)

def wallet_histo_line_vente(wallet, shares, h):
    wallet['especes'] += abs(h['montant'])
    wallet['titres'] -= abs(h['montant'])
    shares[h['code']]['amount'] -= h['quantite']
    if shares[h['code']]['amount'] == 0:
        shares[h['code']]['pru'] = 0
    return (wallet, shares)

def wallet_histo_line_achat(wallet, shares, h):
    wallet['especes'] -= abs(h['montant'])
    wallet['titres'] += abs(h['montant'])
    if not shares[h['code']]['pru'] and h['quantite']:
        shares[h['code']]['pru'] = abs(h['montant']) / h['quantite']
        shares[h['code']]['amount'] += h['quantite']
    elif h['quantite']:
        pru = shares[h['code']]['pru']
        amount = shares[h['code']]['amount']
        amount2 = h['quantite']
        montant = abs(h['montant'])
        if (amount + amount2) == 0:
            newpru = 0
        else:
            newpru = ((pru * abs(amount)) + montant) / (abs(amount) + amount2)
        shares[h['code']]['pru'] = newpru
        shares[h['code']]['amount'] = amount + amount2
        if shares[h['code']]['amount'] == 0:
             shares[h['code']]['pru'] = 0
    return (wallet, shares)

def wallet_histo_line(wallet, shares, h):
    if h['code'] not in shares.keys():
        shares[h['code']] = {
            'code': h['code'],
            'amount': 0,
            'pru': 0,
            'isin': h['isin'],
            'devise': h['devise'],
        }
        # CA shares hotfix
        shares[h['code']] = ca_fix(shares[h['code']])
    if h['operation'] == "VERSEMENT" and h['type'] == 'E':
        wallet, shares = wallet_histo_line_versement(wallet, shares, h)
    elif h['operation'] == "RETRAIT" and h['type'] == 'E':
        wallet, shares = wallet_histo_line_retrait(wallet, shares, h)
    elif h['operation'] == "FRAIS":
        wallet, shares = wallet_histo_line_frais(wallet, shares, h)
    elif h['operation'] == "DIVIDENDE":
        wallet, shares = wallet_histo_line_dividende(wallet, shares, h)
    elif h['operation'] == "ACHAT" and h['type'] == 'T':
        wallet, shares = wallet_histo_line_achat(wallet, shares, h)
    elif h['operation'] == "VENTE" and h['type'] == 'T':
        wallet, shares = wallet_histo_line_vente(wallet, shares, h)
    wallet['total'] = wallet['titres'] + wallet['especes']
    return (wallet, shares)

def wallet_histo_parse(wallet, histo):
    shares = {}
    for h in histo:
        wallet, shares = wallet_histo_line(wallet, shares, h)
    values = []
    for code in shares:
        if shares[code]['amount'] != 0:
            values.append({
                'code': None if code == '-' else code,
                'amount': shares[code]['amount'],
                'pru': shares[code]['pru'],
                'isin': shares[code]['isin'],
                'currency': shares[code]['devise'],
            })
    return (wallet, histo, values)

def csv_to_wallet(filename):
    wallet = {
        'versements': 0.00,
        'total': 0.00,
        'especes': 0.00,
        'titres': 0.00,
        'perf': 0.00,
        'values': [],
    }
    with open(filename, 'r') as fd:
        buf = fd.read()
        lines = buf.split('\n')
    wallet['name'] = get_wallet_name(lines)
    wallet['filename'] = wallet['name'].replace('.csv', '.json')
    wallet['updated'] = get_wallet_date(lines)
    histo = get_wallet_histo(lines)
    wallet, histo, values = wallet_histo_parse(wallet, histo)
    for elem in values:
        print(elem['code'], elem['amount'], '@', "%.2f" % elem['pru'], elem['currency'])
    wallet['total'] = wallet['titres'] + wallet['especes']
    wallet['values'] = values
    print("Historique: %d élément(s)" % len(histo))
    return (wallet)

def generate_wallet(filename):
    wallet = csv_to_wallet(filename)
    out = json.dumps(wallet, indent=4)
    with open(wallet['filename'], 'w') as fd:
        fd.write(out)
        print("JSON file '%s' written." % wallet['filename'])


##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()
    else:
        for arg in sys.argv[1:]:
            generate_wallet(arg)
