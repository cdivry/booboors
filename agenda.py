#!/usr/bin/env python3

import requests
import json
import datetime
import sys
import os

API_ENDPOINT = 'https://finnhub.io/api/v1/calendar/ipo?from=%(from)s&to=%(to)s&token=%(token)s'
API_TOKEN='XXX'

def timestamp_to_date_format(ts):
    date = datetime.datetime.fromtimestamp(ts / 1000)
    return (date.strftime("%Y-%m-%d"))

def get_ipo(frm, to):
    events = []
    url = API_ENDPOINT % {
        'from': frm,
        'to': to,
        'token': API_TOKEN
    }
    print(url)
    res = requests.get(url)
    val = json.loads(res.text)

    if 'ipoCalendar' in val:
        for ipo in val['ipoCalendar']:
            events.append({
                'date': ipo['date'],
                'name': 'IPO @ ' + ipo['name'],
            })

    #print(val)
    return (events)


def get_witches(frm, to):

    date = datetime.datetime.strptime(frm, "%Y-%m-%d")
    until = datetime.datetime.strptime(to, "%Y-%m-%d")
    events = []
    curr_month = 0
    fridays = 0
    while date <= until:
        if date.month != curr_month:
            fridays = 0
        if date.weekday() == 4:
            fridays += 1
            if fridays == 3:
                events.append({
                    'date': date.strftime("%Y-%m-%d"),
                    'name': 'Vendredi des 3 sorcières',
                })
        curr_month = date.month
        date += datetime.timedelta(days=1)
    return (events)

def save_events(events):
    if not os.path.exists('./data/'):
        os.mkdir('./data/')
    if not os.path.exists('./data/agenda/'):
        os.mkdir('./data/agenda/')
    with open('./data/agenda/' + frm + '.json', 'w') as fd:
        fd.write(json.dumps(events))


if __name__ == '__main__':

    events = []

    now = datetime.datetime.now()
    frm = now.strftime("%Y-%m-01")
    to = frm.replace(str(now.year), str(now.year + 1))

    events += get_ipo(frm, to)
    events += get_witches(frm, to)

    events = sorted(events, key=lambda k: k['date'])
    save_events(events)

    for e in events:
        print(e)

