#!/usr/bin/env python3

##########################################
## IMPORTS                              ##
##########################################

import json
import datetime
from zonebourse import zb_query

##########################################
## VARIABLES                            ##
##########################################

CURR = {
    'EUR': '€',
    'CHF': 'F',
    'PLN': 'Z',
}

##########################################
## FUNCTIONS                            ##
##########################################

def get_curr(value):
    if 'currency' in value and value['currency']:
        if value['currency'] in CURR:
            return (CURR[value['currency']])
    return ('?')

def html_value(value):
    html_class = ['element']
    if 'sell_price' in value and value['price'] > value['sell_price']:
        html_class.append('tut')
    if 'buy_price' in value and value['price'] < value['buy_price']:
        html_class.append('tut')
    for item in ['code','prefix','isin']:
        if item not in value or not value[item]:
            html_class.append('bug')
    if value['prefix'] == 'ZB':
        zb = zb_query(value['isin'])
        link = zb['page']
    elif not value['prefix'] or not value['code']:
        link = ''
    else:
        link = "https://www.boursorama.com/cours/" + value['prefix'] + value['code']
    curr = get_curr(value)
    html = """
    <tr class="%(html_class)s">
      <td name="code">%(code)s</td>
      <td name="price">%(price)s</td>
      <td name="perf" style="color: %(color)s">%(perf)s %%</td>
      <td name="pru">%(pru)s</td>
      <td name="amount">%(amount)s</td>
      <td name="valo">%(valo)s</td>
      <td name="pv" style="color: %(color)s">%(pv)s</td>
      <td name="name"><a href="%(link)s" target="_blank">%(name)s</a></td>
    </tr>
    """ % {
        'html_class': ' '.join(html_class),
        'color': "#080" if value['perf'] >= 0 else "#800",
        'code': value['code'],
        'price': "%.2f %s" % (value['price'], curr),
        'perf': "%.2f" % value['perf'],
        'pru': "%.2f %s" % (value['pru'], curr),
        'amount': "%d" % value['amount'],
        'valo': "%.2f %s" % (value['valo'], curr),
        'pv': "%.2f %s"  % (value['pv'], curr),
        'name': value['name'],
        'link': link,
    }
    return (html)

def html_wallet(wallet):
    wallet_values = ""
    for v in wallet['values']:
        wallet_values += html_value(v)
    col_perf = "#0f0" if wallet['perf'] >= 0.0 else "#f00"
    sig_perf = "+" if wallet['perf'] >= 0.0 else ""
    updated = datetime.datetime.fromtimestamp(wallet['updated'])
    updated = updated.strftime('le %d-%m-%Y à %H:%M')
    liquid = wallet['especes'] * 100.0 / wallet['total']

    html = """
    <div class="wallet">
      <h2>%(name)s %(perf)s</h2>
      <span class="wallet_infoline">Mis à jour %(updated)s</span>
      <span class="wallet_infoline">Liquide à %(liquid)s %%</span>
       <span class="details">
        <b>Total</b>: %(total)s € -
        <b>Titres</b>: %(titres)s € -
        <b>Espèces</b>: %(especes)s € -
        <b>Versements</b>: %(versements)s €
       </span>
       <hr>
       <table>
        <thead>
         <tr>
          <th>Code</th>
          <th>Prix</th>
          <th>Performance</th>
          <th>P.R.U.</th>
          <th>Nombre</th>
          <th>Valorisation</th>
          <th>Plus/Moins Value</th>
          <th>Nom</th>
         </tr>
        </thead>
        <tbody>
          %(values)s
        </tbody>
       </table>
    </div>
    """ % {
        'name': wallet['name'],
        'updated': updated,
        'total': "%.2f" % wallet['total'],
        'titres': "%.2f" % wallet['titres'],
        'especes': "%.2f" % wallet['especes'],
        'versements': "%.2f" % wallet['versements'],
        'liquid': "%.2f" % liquid,
        'perf': "<span style='color: %s'>" % col_perf + sig_perf + "%.2f" % wallet['perf'] + ' %' + "</span>",
        'values': wallet_values,
    }
    return (html)

def html_specific(wallet):
    with open('./www/template.html') as fd:
        template = fd.read()
    html = template % {
        'HTML': html_wallet(wallet),
        'FILENAME': wallet['filename'].replace('.json', '.html'),
        'NAME': wallet['name'],
    }
    return (html)

def html_index(wallets):
    with open('./www/template.html') as fd:
        template = fd.read()
    html_content = ""
    for w in wallets:
        html_content += html_wallet(w)
    html = template % {
        'HTML': html_content,
        'FILENAME': 'index.html',
        'NAME': "INDEX",
    }
    return (html)
